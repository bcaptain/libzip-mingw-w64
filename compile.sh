# this is just a script to help compile libzip+zlib, because it's a real pain in the ass to do so.
# before using this script, create a new directory, stick this script in it, extract libzip and zlib
# sources into their own independent subdirectories, update the following two variables (or more)
# and run the script. 
# When things go wrong, delete the build directoires or even the source directories and start over.

# all the following variables were set to compile libzip with zlib on windows using msys.

##################################################################
# I will now document my attempts in trying to compile libzip+zlib
##################################################################

# libzip from the presumably official repo at https://github.com/aburgh/libzip appears to just suck. There's one commit, one
# release, and it hasn't been touched for 7 years. What the hell? Not using this.

# libzip from https://github.com/nih-at/libzip is active within the last 17 days. Let's use this.

# trying zlib git tags:
# 1.2.9 fails during cmake: undefined reference to _wopen
# 1.2.8 appears to compile fine. going to usethis

# errors <= tag rel-1-5-2 often err "ZLIB version too old, please install at least v1.1.2" no matter what. just rip out taht check from CMakeLists.txt, they do this in later versions anyway.

# OLDER VERSIONS
# compiling then dies with : error: conflicting types for ‘zip_source_win32w_create’
# going to just try to fix it.
# fixed it, created a branch rel-1-5-2_antic. Compiles, but won't link.
# I applied the same fix to rel-1-6-1, merged the changes to a new branch rel-1-6-1_antic. It also appears in newer editions, they've also simply ripped out the zlib version check.
# compiles but doesn't link, seems the project maintainers for libzip forgot to include zip_source_win32w.c in CMakeLists.txt.
# ditto for zip_random_win32.c and zip_random_uwp.c. The latter is presumably "universal windows platform", and may be 64bit as opposed to 32, or maybe just a newer implementation for Windows 10. 
# _wcsdup and _snwprintf are defined in system headers somewhere. #include <wchar.h> in zip_source_win32w.c
# that didn't work, still undefined. grepping for _snwprintf in /usr/include shows it exists in /usr/include/w32api/strsafe.h, so... aha, let's just #include <windows.h> and see what happens.
# still not finding the symbols.

### update: newer versions of zlib apparently change from libz to libzlib. makes more sense now.

ZLIBPATH="./zlib" #careful to make this correct, rm -rf is used on the build directory if the directory exists
LIBZIPPATH="./libzip" #careful to make this correct, rm -rf is used on the build directory if the directory exists
PROJECT="-GCodeLite - Unix Makefiles" # set to "" to compile the program instead of genereate a project
CORES=8
STATIC=OFF
BUILD_TESTS=ON

ENABLE_COMMONCRYPTO=OFF
ENABLE_GNUTLS=OFF
ENABLE_MBEDTLS=OFF
ENABLE_OPENSSL=OFF
ENABLE_WINDOWS_CRYPTO=OFF
ENABLE_BZIP2=OFF
ENABLE_LZMA=OFF
BUILD_TOOLS=OFF
BUILD_REGRESS=OFF
BUILD_EXAMPLES=OFF
BUILD_DOC=OFF

DO_GUTS=0
DO_DEFLATE=0
DO_ZIPINT=0
DO_ZIPCMP=0
DO_GUTS=0
DO_WINDRES=0
DO_ZCONF_MOVE=0
DO_ZCONF_DELETE=0
DO_CONFIGLOC=1
DO_ZLINKS=0
DO_ZCOPIES=0
USE_MAKE=1 # set to 0 to use cmake to do the building

DCMAKE_SYSTEM_NAME="Windows"

# Unix Makefiles               = Generates standard UNIX makefiles.
# Ninja                        = Generates build.ninja files.
# Ninja Multi-Config           = Generates build-<Config>.ninja files.
# CodeBlocks - Ninja           = Generates CodeBlocks project files.
# CodeBlocks - Unix Makefiles  = Generates CodeBlocks project files.
# CodeLite - Ninja             = Generates CodeLite project files.
# CodeLite - Unix Makefiles    = Generates CodeLite project files.
# Sublime Text 2 - Ninja       = Generates Sublime Text 2 project files.
# Sublime Text 2 - Unix Makefiles
#                              = Generates Sublime Text 2 project files.
# Kate - Ninja                 = Generates Kate project files.
# Kate - Unix Makefiles        = Generates Kate project files.
# Eclipse CDT4 - Ninja         = Generates Eclipse CDT 4.0 project files.
# Eclipse CDT4 - Unix Makefiles= Generates Eclipse CDT 4.0 project files.

if [ "$DO_GUTS" -ne "0" ] ; then	
	FILE="$ZLIBPATH/gzguts.h"
	echo "#### ZLib #### Fixing $FILE"
	if [ ! -f "$FILE" ] ; then
		echo "#### ZLib #### cant find: $FILE"
		exit 1	
	fi
	sed -i 's/defined(_WIN32) || defined(__CYGWIN__)/defined(_WIN32)/g' "$FILE"
	if [ "$?" -ne "0" ] ; then
		echo "#### ZLib #### could not process $FILE"
		exit 1
	fi
fi

if [ "$DO_WINDRES" -ne "0" ] ; then
	FILE="$ZLIBPATH/win32/zlib1.rc"
	echo "#### ZLib #### Fixing $FILE"
	if [ ! -f "$FILE" ] ; then
		echo "#### ZLib #### cant find: $FILE"
		exit 1	
	fi
	sed -i 's/#ifdef GCC_WINDRES/#ifndef GCC_WINDRES/g' "$FILE"
	if [ "$?" -ne "0" ] ; then
		echo "#### ZLib #### could not process $FILE"
		exit 1
	fi
fi

if [ "$DO_ZCONF_DELETE" -ne "0" ] ; then
	echo #### ZLib #### Deleting zipcconf.h because zlib demands it."
	rm -f $ZLIBPATH/zconf.h
fi

echo "#### ZLib #### building"

FILE="$ZLIBPATH/build"
rm -rf "$FILE"
mkdir "$FILE"
cd "$FILE"
if [ "$?" -ne "0" ] ; then
	echo "#### ZLib #### cant change to directory: $FILE"
	exit 1	
fi

cmake -DCMAKE_SYSTEM_NAME="Windows" .. \
&& make CXXFLAGS="-v -Wl,--verbose" CFLAGS="-v -Wl,--verbose" VERBOSE=999 -j"$CORES"

if [ "$?" -ne "0" ] ; then
	echo "#### ZLib #### cmake failed"
	exit 1	
fi

if [ "$DO_ZLINKS" -ne "0" ] ; then
	ln -s libzlib.dll.a zlib.dll.a
	ln -s libzlib.dll.a libz.dll.a
	ln -s libzlib.dll zlib.dll
	ln -s libzlib.dll libz.dll
	ln -s libzlibstatic.a zlibstatic.a
	ln -s libzlibstatic.a zlib.a
	ln -s libzlibstatic.a libz.a
fi

cd -

if [ "$?" -ne "0" ] ; then
	echo "#### ZLib #### cant change back to previous directory: $FILE"
	exit 1	
fi

if [ "$DO_ZCONF_MOVE" -ne "0" ] ; then
	echo #### ZLib #### Copying zipcconf.h to where libzip expects it."
	cp $ZLIBPATH/build/zconf.h $ZLIBPATH/zconf.h
	if [ "$?" -ne "0" ] ; then
		echo "copy failed"
		exit 1	
	fi
fi

SEDEXP="s/<zlib.h>/\"..\/..\/${ZLIBPATH/\//\\\/}\/zlib.h\"/g"

if [ "$DO_ZIPINT" -ne "0" ] ; then
	FILE1="$LIBZIPPATH/lib/zipint.h"
	if [ ! -f "$FILE1" ] ; then
		echo "cant find: $FILE1"
		exit 1	
	fi
	echo "#### LibZip #### Fixing $FILE1"

	sed -i "$SEDEXP" "$FILE1"
	if [ "$?" -ne "0" ] ; then
		echo "#### LibZip #### could not process $FILE1"
		exit 1
	fi
fi

if [ "$DO_DEFLATE" -ne "0" ] ; then
	FILE2="$LIBZIPPATH/lib/zip_algorithm_deflate.c"
	echo "#### LibZip #### Fixing $FILE2"
	sed -i "$SEDEXP" "$FILE2"
	if [ "$?" -ne "0" ] ; then
		echo "#### LibZip #### could not process $FILE2"
		exit 1
	fi
fi

if [ "$DO_ZIPCMP" -ne "0" ] ; then
	FILE3="$LIBZIPPATH/src/zipcmp.c"
	if [ ! -f "$FILE3" ] ; then
		echo "#### LibZip #### cant find: $FILE3"
		exit 1	
	fi
	echo "#### LibZip #### Fixing $FILE3"
	sed -i "$SEDEXP" "$FILE3"
	if [ "$?" -ne "0" ] ; then
		echo "#### LibZip #### could not process $FILE3"
		exit 1
	fi
fi

#echo #### LibZip #### correcting config location"

## cmake seems to be ignoring the compiler flags, so let's just use sed.
#-DCXXFLAGS="-isystem ../build -I ../build -I ../lib" \
#-DCFLAGS="-isystem ../build -I ../build -I ../lib" \

if [ "$DO_CONFIGLOC" -ne "0" ] ; then
	sed -i 's/"config.h"/"..\/build\/config.h"/g' ./libzip/lib/compat.h
	sed -i 's/"config.h"/"..\/build\/config.h"/g' ./libzip/lib/zipint.h
	sed -i 's/"config.h"/"..\/build\/config.h"/g' ./libzip/src/zipcmp.c
	sed -i 's/"config.h"/"..\/build\/config.h"/g' ./libzip/src/zipmerge.c
	sed -i 's/"config.h"/"..\/build\/config.h"/g' ./libzip/src/ziptool.c
	sed -i 's/"config.h"/"..\/build\/config.h"/g' ./libzip/regress/can_clone_file.c
	sed -i 's/"config.h"/"..\/build\/config.h"/g' ./libzip/regress/fread.c
	sed -i 's/"config.h"/"..\/build\/config.h"/g' ./libzip/regress/hole.c
	sed -i 's/"config.h"/"..\/build\/config.h"/g' ./libzip/regress/malloc.c
	sed -i 's/"config.h"/"..\/build\/config.h"/g' ./libzip/regress/tryopen.c
	sed -i 's/"config.h"/"..\/build\/config.h"/g' ./libzip/regress/nonrandomopen.c
	sed -i 's/"compat.h"/"..\/lib\/compat.h"/g' ./libzip/regress/nonrandomopen.c
	sed -i 's/"zipconf.h"/"..\/build\/zipconf.h"/g' ./libzip/regress/nonrandomopen.c
	sed -i 's/"zipint.h"/"..\/lib\/zipint.h"/g' ./libzip/regress/nonrandomopen.c
	sed -i 's/"zipconf.h"/"..\/build\/zipconf.h"/g' ./libzip/lib/compat.h
	sed -i 's/<zipconf.h>/"..\/build\/zipconf.h"/g' ./libzip/lib/zip.h
fi

echo #### LibZip #### building"

FILE="$LIBZIPPATH/build"
rm -rf "$FILE"
mkdir "$FILE"
cd "$FILE"
if [ "$?" -ne "0" ] ; then
	echo "#### LibZip #### cant change to directory: $FILE"
	exit 1	
fi

ZDIR="../../$ZLIBPATH/build"
stat "$ZDIR" 2>/dev/null 1>/dev/null
if [ "$?" -ne "0" ] ; then
	echo "#### LibZip #### cant find directory: $ZDIR"
	exit 1	
fi

# -DZLIB_LIBRARY="$ZDIR/libzlib.dll.a" \

	#-DZLIB_LIBRARY="$ZLIB_LIBRARY" \
	
	#ZLIB_LIBRARY='e:\\monticello\\libzip-git\\zlib\\build\\libzlib.dll.a' # this MUST be absolute path if building on windows

#-DCMAKE_PREFIX_PATH="$ZDIR" \

# we're turning off OpenSSL and BZip2 - things that cmake tries to compile against in the msys system

if [ "$DO_ZCOPIES" -ne "0" ] ; then
	# # I'm just going to copy everything into the build dir because I can't get CMake to find it anywhere else.
	# echo "########## pwd"
	# pwd
	# echo "########## ls"
	# ls 
	# echo "########## ls .."
	# ls ..
	# echo "########## ls ../.."
	# ls ../..
	# echo "########## ls ../../zlib"
	# ls ../../zlib
	# echo "########## ls ../../zlib/build"
	# ls ../../zlib/build
	echo "########## Copying the following files to "`pwd`
	ls -l "$ZDIR/"*.dll "$ZDIR/"*.a "$ZDIR/"*.h "$ZDIR/"*.pc "$ZDIR/"*.obj
	cp "$ZDIR/"*.dll "$ZDIR/"*.a "$ZDIR/"*.h "$ZDIR/"*.pc "$ZDIR/"*.obj "./"
fi

# this just doesn't work
#-DLINK_DIRECTORIES="$ZDIR VERBOSE=1" \
#-CMAKE_LIBRARY_PATH="$ZDIR VERBOSE=1" \
#-DBUILD_STATIC="$STATIC" \
#-DBUILD_TESTS=ON \

cmake "$PROJECT" \
-DBUILD_STATIC=$STATIC \
-DZLIB_INCLUDE_DIR="$ZDIR" \
-DZLIB_LIBRARY='zlib' \
-DCMAKE_PREFIX_PATH="$ZDIR" \
-DENABLE_BZIP2=OFF \
-DENABLE_OPENSSL=OFF \
-DCMAKE_SYSTEM_NAME="$DCMAKE_SYSTEM_NAME" \
-DCMAKE_VERBOSE_MAKEFILE:BOOL=ON \
-DENABLE_COMMONCRYPTO=OFF \
-DENABLE_GNUTLS=OFF \
-DENABLE_MBEDTLS=OFF \
-DENABLE_OPENSSL=OFF \
-DENABLE_WINDOWS_CRYPTO=OFF \
-DENABLE_BZIP2=OFF \
-DENABLE_LZMA=OFF \
-DBUILD_TOOLS=OFF \
-DBUILD_REGRESS=OFF \
-DBUILD_EXAMPLES=OFF \
-DBUILD_DOC=OFF \
..

if [ "$?" -ne "0" ] ; then
	echo "#### LibZip #### cmake failed"
	exit 1	
fi

if [ "$PROJECT" == "" ] ; then	
	if [ "$USE_MAKE" -ne "0" ] ; then	
		make \
		CXXFLAGS="-v -Wl,--verbose" \
		CFLAGS="-v -Wl,--verbose" \
		VERBOSE=999 \
		-j"$CORES"
	else
		cmake --build . --config Release
	fi
fi

#CXXFLAGS="-isystem ../build -I ../build -I ../lib -v -Wl,--verbose" \
#CFLAGS="-isystem ../build -I ../build -I ../lib -v -Wl,--verbose" \

if [ "$?" -ne "0" ] ; then
	echo "#### LibZip #### cmake failed"
	exit 1	
fi

# cmake --build . --config Release

